from fastapi import FastAPI
from item_manager.item_manage import item_manage
from contextlib import asynccontextmanager

@asynccontextmanager
async def lifespan(app: FastAPI):
    print("working the before launch part")
    yield
    print("working the after exit part")

app = FastAPI(lifespan=lifespan)
app.include_router(item_manage,prefix="/item_manage",tags=["item_manage"])

