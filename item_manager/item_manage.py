from typing import Union
from fastapi import FastAPI, Header, Depends,Body,Query,BackgroundTasks, APIRouter
from pydantic import BaseModel
import time
from contextlib import asynccontextmanager
import re

pattern = r"Token\s+([\w-]+)"
item_manage = APIRouter()
    
class Item(BaseModel):
    name:str
    price:float
    buyers:list = []
    dicto:dict = {}
    is_offer:Union[bool,None]=None

class User(BaseModel):
    username:str
    password:str

fake_db = [
    {
    "username":"arjunp",
    "password":"1234",
    "Token":"12345"
    },
    {
    "username":"john",
    "password":"1234",
    "Token":"12346"
    }
]


def get_current_user():
    return {"user":"john"}

def check_header(Authorization:str = Header(default=None)):
    if Authorization is None:
        return False
    else:
        match = re.search(pattern,Authorization)
        if match:
            print(token:=match.group(1))
            for entries in fake_db:
                if entries["Token"] == token:
                    return entries["username"]
        else:
            return False

def add_numbers(number1,number2):
    time.sleep(5)
    print("the answer is",number1+number2)  
    return number1 + number2

@item_manage.get("/")
async def read_root():
    return {"message":"hi there"}

@item_manage.get("/get-token")
async def get_token(user:User = Body(...,description="User data as request body")):
    for entries in fake_db:
        if entries["username"] == user.username and entries["password"] == user.password:
            return {"Token":entries["Token"]}
    return {"message":"no user found"}        

@item_manage.get("/check_user")
async def check_user(username:str = Depends(check_header)):
    if username:
        return {"username":username}
    else:
        return {"message":"no user found or no authorization found"}

@item_manage.get("/try_background_tasks/{number1}/{number2}")
async def try_background_tasks(number1: int, number2: int,background_tasks: BackgroundTasks):
    background_tasks.add_task(add_numbers,number1,number2)
    return {"message":"added to background tasks"}




