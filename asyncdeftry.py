import asyncio
import time
import httpx
import json

url = 'http://universities.hipolabs.com/search'

async def get_all_universities_for_country_async(country: str, data: dict) -> None:
    params = {'country': country}
    async with httpx.AsyncClient() as client:
        response = await client.get(url, params=params)
        response_json = json.loads(response.text)
        universities = []
        for university in response_json:
            universities.append({
                'name': university['name'],
                'domains': university['domains']
            })
    data[country] = universities


async def get_universities_async() -> dict:

    data: dict = {}
    await asyncio.gather(get_all_universities_for_country_async("turkey", data),
                         get_all_universities_for_country_async("india", data),
                         get_all_universities_for_country_async("australia", data))
    print(data)

asyncio.run(get_universities_async())
